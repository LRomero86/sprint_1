{
  "carrito": [
    {
      "detalleCarrito": [
        {
          "producto": [
            {
              "id": 1,
              "nombreProducto": "Bagel de salmón",
              "precio": 425
            }
          ],
          "cantidad": 4
        },
        {
          "producto": [
            {
              "id": 1,
              "nombreProducto": "Focaccia",
              "precio": 300
            }
          ],
          "cantidad": 4
        }
      ]
    }
  ],
  "formaPago": "string",
  "direccion": "string",
  "estado": "string"
}