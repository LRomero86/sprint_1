const express = require('express');
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const app = express();


app.use(express.json());
app.use(express.urlencoded( { extended: true }));

//configuración de swagger

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Sprint 1',
            version: '1.0.0',
        }
    },
    apis: ['./swagger.js'],
};


// configuración de swaggerDocs

const swaggerDocs = swaggerJSDoc(swaggerOptions);


//configuración de la app "app" para el uso en swagger

app.use('/api-docs',
    swaggerUI.serve,
    swaggerUI.setup(swaggerDocs)
);


/** ARRAY DE USUARIOS **/

var usuarios = [
    {
        id: 1,
        user:"admin",
        otrakey:"",
        pass:"1234",
        admin:true,
        is_logged:false,
    },
    {
        id: 2,
        user: "lr",
        nom_ape: "Luciano Romero",
        email: "lr@mail.com",
        tel: 445566,
        direccion: "dire 123",
        pass:"123",
        admin:false,
        is_logged:false,
    }
   /** {
        id:Number,
        user:String,
        nom_ape:String,
        email:String,
        tel:Number,
        direccion:String,
        pass:String,
        admin:Boolean,
        is_logged:false,
    }*/
];


/** ARRAY DE PRODUCTOS **/

var productos = [
    {
        id:1,
        nombreProducto:"Bagel de salmón",
        precio:425,
        is_delete: false,
    },
    {
        id:2,
        nombreProducto:"Hamburguesa clásica",
        precio:350,
        is_delete: false,
    },
    {
        id:3,
        nombreProducto:"Sandwich veggie",
        precio:310,
        is_delete: false,
    },
    {
        id:4,
        nombreProducto:"Ensalada veggie",
        precio:340,
        is_delete: false,
    },
    {
        id:5,
        nombreProducto:"Focaccia",
        precio:300,
        is_delete: false,
    },
    {
        id:6,
        nombreProducto:"Sandwich focaccia",
        precio:440,
        is_delete: false,
    },
    {
        id:7,
        nombreProducto:"Ensalada focaccia",
        precio:440,
        is_delete: false,
    }
];

/** ARRAY DE PEDIDOS **/

var pedidos = [];


/** ARRAY DE FORMA DE PAGO **/

var formaPago = [
    {
        id:1,
        tipo:"Efectivo",
        is_delete:false,
    },
    {
        id:2,
        tipo:"Débito",
        is_delete:false,
    },
    {
        id:3,
        tipo:"Crédito",
        is_delete:false,
    }
];


/** MIDDLEWARES **/


/** Validación de login **/
function validarLogin(req, res, next) {

    let found = usuarios.find(usr => usr.is_logged == true);
    
    if(!found) {

        let userLogin;
    
        if(!req.body.user || !req.body.pass) {
            //console.log('están vacios');
            res.status(401).send("SE REQUIEREN TODOS LO CAMPOS");
        } else {
            usuarios.map((item, index) => {
                console.log("index: ", index);
                if (item.user.toLowerCase() === req.body.user.toLowerCase() && item.pass === req.body.pass) {
                    //console.log(`user es: ${item.user}`);
                    item.is_logged = true;
                    userLogin = item;
                    next();
                }
            });
            
            !userLogin ? res.send('Usuario NO válido'):"";
        }
    } else {
        res.send('Ya existe un usuario logueado');
    }
}

/** IsLogged **/
function isLogged(req, res, next) {

    const userFound = usuarios.find(usr => usr.is_logged == true);
    
    if(!userFound) {

        res.send('Por favor inicie sesión o regístrese');
        
    } else {
        //console.log('logueado: ',userFound);
        //res.locals.userLogin;
        res.locals.usrIsLogged = userFound;
        next();
    }
}

/** isAdmin **/
function isAdmin(req, res, next) {

    let userIsLogged = res.locals.usrIsLogged;

    if(userIsLogged.admin == true) {
        res.locals.usrIsAdmin = userIsLogged;
        next();
    } else {
        res.send('El usuario no es admin');
    }
}

/** Registrar usuarios **/
function registrarUsuario(req, res, next) {

    if(!req.body.user || !req.body.nom_ape || !req.body.email || !req.body.tel || !req.body.direc || !req.body.pass){
        res.send('por favor complete todos los campos');
    } else {
        console.log('los datos son válidos');        
        next();
    }    
}

/** Validar email **/
function validarEmail(req, res, next) {
    
    let found = usuarios.find(usr => usr.email == req.body.email);
    if(!found){
        console.log('Email válido');
        next();
    } else {
        console.log('Email ya registrado');
        res.send('email ya existe');
    }
}

/** Validar producto **/
function validarProducto(req, res, next) {
    
    let found = productos.find(prod => prod.nombreProducto == req.body.nombreProducto);
    if(!found){
        console.log('Plato cargado correctamente');
        next();
    } else {
        console.log('plato ya registrado');
        res.send('Plato ya existe');
    }
}

/** Borrar producto **/
function eliminarProducto(req, res, next) {
    
    let prodFound = productos.find(prod => prod.id == req.body.id);
    if(!prodFound){
        console.log('No existe plato');
        res.send('No existe plato');
    } else {
        console.log('se encuentra plato');
        next();
    }
}

/** Validar pedido **/
function validarPedido(req, res, next) {

    const pedidoRealizado = req.body;
    const prod = pedidoRealizado.carrito[0].detalleCarrito;
    var verProd;
    var flag = true;
    var valCant;
    var costoTotal =0.0;

    prod.map((item) => {
        //console.log("nombreProducto Item: ", item.producto[0].nombreProducto);
        verProd = productos.find(itemComp => itemComp.nombreProducto == item.producto[0].nombreProducto)
        //    console.log("DENTRO DEL FIND", verProd);
            if(!verProd) {
                flag = false;
            }
    });


    if(flag == false) {
        res.send("Por favor seleccione un plato existente");
    } else {
        prod.map((item) => {
        //    console.log("Cantidad Item: ", item.producto[0].nombreProducto);
            if(item.cantidad < 1) {
                valCant = item;
            }
            console.log("cantidad: ", item.cantidad);
            console.log("precio: ", item.producto[0].precio);
            costoTotal = parseFloat(costoTotal) + (parseFloat(item.cantidad) * parseFloat(item.producto[0].precio));
        });
        if(!valCant){
            res.locals.costoTotal = costoTotal;
            res.locals.pedidoRealizado = pedidoRealizado;
            console.log("COSTO: ", costoTotal);
            next();
        } else {
            console.log("ingrese una cantidad válida");
                res.send("por favor ingrese una cantidad válida");
        }
    }
}

/** Validar formaPago **/
function validarFormaPago(req, res, next) {

    const pedidoRealizado = res.locals.pedidoRealizado;

    var valFormaPago = formaPago.find(fp => fp.tipo == pedidoRealizado.formaPago);

    if(!valFormaPago) {
        console.log("Por favor ingrese una forma de pago válida");
        res.send("Por favor ingrese una forma de pago válida");
    } else {
        next();
    }    
}

/** Validar dirección pedido **/
function validarDirPedido(req, res, next) {
    
    const pedidoRealizado = res.locals.pedidoRealizado;
    const usrId = res.locals.usrIsLogged;
    
    if(pedidoRealizado.direccion == "") {
        pedidoRealizado.direccion = usrId.direccion;
    }

    next();
}

/** Modificar estado de pedidos desde usr admin **/
function buscarPedido(req, res, next) {

    bPedido = req.body;

    const pedidoFound = pedidos.find(pe => pe.id == bPedido.id);

    if(!pedidoFound) {
        res.send("No existe pedido")
    } else {
        res.locals.pedidoFound = pedidoFound;
        next();
    }
    
}

/** Validar registro de nueva forma de pago **/
function validarNuevaFormaPago(req, res, next) {

    fPago = req.body.formaPago;

    let formaPagoFound = formaPago.find(fp => fp.tipo.toLowerCase() == fPago.toLowerCase())

    if(!formaPagoFound) {
        next();
    } else {
        res.send('Ya existe esa forma de pago');
    }
}

/** Validar búsqueda de forma de pago **/
 function buscarFormaPago(req, res, next) {

    bFormaPago = req.body;

    const fpFound = formaPago.find(fp => fp.id == bFormaPago.id);

    if(!fpFound) {
        res.send("No existe forma de pago");
    } else {
        res.locals.fpFound = fpFound;
        next();
    }
 }


/** FIN MIDDLEWARES **/



/**
* REGISTRAR USUARIO
*/            

 app.post("/usuarios/registrar", registrarUsuario, validarEmail, function(req,res) {
    
    usrId = usuarios.length + 1;
    usuarios.push(
        {
            id: usrId,
            user: req.body.user,
            nom_ape: req.body.nom_ape,
            email: req.body.email,
            tel: req.body.tel,
            direccion: req.body.direccion,
            pass: req.body.pass,
            admin: false,
            is_logged:false,
        }
    );
    console.log(usuarios);
    res.status(200).send('Registrado Exitosamente');
});


/**
 * LOGIN
 */

app.post('/login', validarLogin, function(req, res) {
    //aquí tengo que ingresar mis datos de login
    login = {
        user:req.body.user,
        pass:req.body.pass,
    }    
    res.send('es válido');
});


/**
 * USUARIOS
 */

app.get('/usuarios', isLogged, isAdmin, function(req, res) {

    if(!usuarios) {
        console.log('No existen usuarios');
        res.status(201).send('No existen usuarios');
    } else {
        console.log(usuarios);
        res.send(usuarios);
    }
})


/**
* PRODUCTOS
*/

app.post('/productos/alta_producto', isLogged, isAdmin, function(req, res) { //alta de productos siempre y cuando un user sea admin
    
    idProd = productos.length + 1;
        productos.push(
            {
                id:idProd,
                nombreProducto:req.body.nombreProducto,
                descripcion:req.body.descripcion,
                precio:req.body.precio,
                is_delete:false,
            }                
        );

    console.log('todo vÁLIDO');

    res.send(productos);
})


app.get('/productos', isLogged, function(req, res) {
    if(!productos) {
        console.log('No existen productos');
        res.status(201).send('No existen productos');
    } else {
        const prod = productos.filter(pr => pr.is_delete == false);
        console.log(prod);
        res.send(prod);
    }
})


app.put('/productos/:id',isLogged, isAdmin, function(req, res) {
    
    const prodMod = {id, nombreProducto, descripcion, precio} = req.body;
    productos.map((item) => {        
        if(item.id == prodMod.id) {
            item.nombreProducto = prodMod.nombreProducto;
            item.descripcion = prodMod.descripcion;
            item.precio = prodMod.precio;
            item.is_delete = false;
        }
    });
    
    res.status(200).send(productos);
})


 app.delete('/productos/:id', isLogged, isAdmin, eliminarProducto, function(req, res) {

    let prodFound = productos.find(prod => prod.id == req.body.id);

    prodFound.is_delete = true;
    //productos.splice(req.body.id-1, 1);    
    console.log('Plato eliminado');
    res.send(productos);

})

/**
 * PEDIDOS
 */


app.post('/nuevo_pedido', isLogged, validarPedido, validarFormaPago, validarDirPedido, function(req, res) {

    const usr = res.locals.usrIsLogged;    
    const costoTotal = res.locals.costoTotal;
    const ped = req.body;
    idPedido = pedidos.length + 1;
    
    ped.id = idPedido;
    ped.usrId = usr.id;
    ped.estado = "Nuevo";

    pedidos.push(ped);

    console.log("Costo total: ", costoTotal);
    res.send(pedidos);

});


app.get('/:userId/pedidos/historial', isLogged, function(req, res) {

    const usr = res.locals.usrIsLogged;
    var pedidosUser = pedidos.filter(peUser => peUser.usrId == usr.id)

    console.log(pedidosUser);
    res.send(pedidosUser)
    

});


/**
 * VER TODOS LOS PEDIDOS COMO ADMIN
 */

app.get("/pedidos", isLogged, isAdmin, function(req, res) {

    res.send(pedidos);

});

/**
 * Cambiar estado de pedidos
 */

app.put('/pedidos/:id', isLogged, isAdmin, buscarPedido, function(req, res) {

    var pedidoFound = res.locals.pedidoFound;

    if(req.body.estado != "Confirmado" && req.body.estado != "En preparación" && req.body.estado != "En camino" && req.body.estado != "Entregado") {
    
        res.send("por favor ingrese un estado válido (Confirmado, En preparación, En camino, Entregado)");
    
    } else {
    
        pedidoFound.estado = req.body.estado;
        res.send(pedidoFound);
    }; 

});


/**
 * FORMAS DE PAGO 
 */

app.post('/forma_pago/agregar_forma_pago', isLogged, isAdmin, validarNuevaFormaPago, function(req, res) {
    
    fpId = formaPago.length + 1;
    formaPago.push(
        fp = {
            id:fpId,
            tipo:req.body.formaPago,
            is_delete: false,
        });
        console.log(formaPago)
    res.send(formaPago);
});

app.get('/forma_pago', isLogged, isAdmin, function(req, res) {

    const fpExiste = formaPago.filter(fp => fp.is_delete == false);
    res.send(fpExiste);

});

app.put('/forma_pago/:id', isLogged, isAdmin, buscarFormaPago, function(req, res) {

    var fpFound = res.locals.fpFound;

    fpFound.tipo = req.body.tipo;

    res.send("Registrada correctamente");
});

app.delete('/forma_pago/:id', isLogged, isAdmin, buscarFormaPago, function(req, res) {

    var fpFound = res.locals.fpFound;

    fpFound.is_delete = true;

    res.send("Forma de pago eliminada");
});



app.post('/logout', isLogged, function(req, res) {

    var userLogout = res.locals.usrIsLogged;
    userLogout.is_logged = false;
    res.send("Sesión finalizada");

});



app.listen(3000, function() {
    console.log('corriendo puerto 3000 en appPedidos');
});
