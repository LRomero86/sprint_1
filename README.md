# sprint_1

Delilah Restó

Este sistema está desarrollado en javaScript, utilizando NodeJS y ExpressJS.
Se encuentra documentado y testeado en Swagger.

REPOSITORIO:
    https://gitlab.com/LRomero86/sprint_1.git

Requerimientos:

Editor de texto (preferentemente VS Code)
- NodeJS
    https://nodejs.org/

EpressJS y Swagger:
- En nuestra terminal nos dirigimos a la carpeta raiz de nuestro programa y tipear lo siguiente:

EJEMPLO:
"C:\sprint_1\npm i nodemon express swagger-UI swagger-jsdoc" + enter


Para  poder testearlo, podemos hacerlo con el comando "npm run dev"
EJEMPLO: 
"C:\sprint_1\npm run dev"

Abrimos un navegador e ingresamos a la siguiente URL:

    http://localhost:3000/api-docs/

Desde ese link se podrá acceder a las pruebas mediante Swagger.

En el archivo "EjemplosPedidos.txt" tenemos un ejemplo de la manera de cargar pedidos para poder testear su funcionamiento.


Desarrollado por Luciano Romero