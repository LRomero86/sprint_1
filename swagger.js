/** DOCUMENTACIÓN DE APP.JS **/


/***** USUARIOS *****/

/** POST **/

/**
 * @swagger
 * /usuarios/registrar:
 *  post:
 *    tags:
 *    - Usuarios
 *    description: Creacion de usuario
 *    parameters:
 *    - name: user
 *      type: string
 *      in: formData
 *      required: false
 *      description : nombre de usuario
 *    - name: nom_ape
 *      type: string
 *      in: formData
 *      required: false
 *      description : nombre y apellido del usuario
 *    - name: email
 *      type: string
 *      in: formData
 *      required: false
 *      description: correo electronico del usuario
 *    - name: tel
 *      type: integer
 *      in: formData
 *      required: false
 *      description: telefono del usuario
 *    - name: direc
 *      type: string
 *      in: formData
 *      required: false
 *      description: direccion del usuario
 *    - name: pass
 *      type: string
 *      in: formData
 *      required: false
 *      description: contraseña del usuario
 *    responses:
 *      200:
 *        Sucess
 */


/** GET **/

/**
 * @swagger
 * /usuarios:
 *  get:
 *    tags:
 *    - Usuarios
 *    description: listado de usuarios registrados
 *    responses:
 *      200:
 *        Sucess
 */

/***** LOGIN DE USUARIOS *****/

/**
 * @swagger
 * /login:
 *  post:
 *    tags:
 *    - Login / Logout
 *    description: Login de usuario
 *    parameters: 
 *    - name: user
 *      type: string
 *      in: formData
 *      required: false
 *      description : user
 *    - name: pass
 *      type: string
 *      in: formData
 *      required: false
 *      description: contraseña del usuario
 *    responses:
 *      200:
 *        Sucess
 */

/** LOGOUT DE USUARIO **/

/**
 * @swagger
 * /logout:
 *  post:
 *    tags:
 *    - Login / Logout
 *    description: Cierre de sesión de usuario
 *    responses:
 *      200:
 *        Sucess
 */


/***** PRODUCTOS *****/

/** POST **/

/**
 * @swagger
 * /productos/alta_producto:
 *  post:
 *    tags:
 *    - Productos
 *    description: Alta de productos
 *    parameters: 
 *    - name: nombreProducto
 *      type: string
 *      in: formData
 *      required: false
 *      description : Nombre del producto
 *    - name: descripcion
 *      type: string
 *      in: formData
 *      required: false
 *      description: Descripción del producto
 *    - name: precio
 *      type: float
 *      in: formData
 *      required: false
 *      description : precio del producto
 *    responses:
 *      200:
 *        Sucess
 */


/** GET **/

/**
 * @swagger
 * /productos:
 *  get:
 *    tags:
 *    - Productos
 *    description: listado de productos registrados
 *    responses:
 *      200:
 *        Sucess
 */


/** PUT **/

/**
 * @swagger
 * /productos/{id}:
 *  put:
 *    tags:
 *    - Productos 
 *    description: Modificación de productos
 *    parameters:
 *    - name: id
 *      type: int
 *      in: formData
 *      required: false
 *      description : id del prod 
 *    - name: nombreProducto
 *      type: string
 *      in: formData
 *      required: false
 *      description : Nombre del producto
 *    - name: descripcion
 *      type: string
 *      in: formData
 *      required: false
 *      description: Descripción del producto
 *    - name: precio
 *      type: Number
 *      in: formData
 *      required: false
 *      description: precio del producto
 *    responses:
 *      200:
 *        Sucess
 */


/** DELETE **/

/**
 * @swagger
 * /productos/{id}:
 *  delete:
 *    tags:
 *    - Productos
 *    description: Modificación de productos
 *    parameters:
 *    - name: id
 *      type: int
 *      in: formData
 *      required: false
 *      description: id del prod
 *    responses:
 *      200:
 *        Sucess
 */



/***** PEDIDOS *****/

/**
 * @swagger
 * /nuevo_pedido:
 *  post:
 *    tags: 
 *    - Pedidos
 *    description: registro de pedidos por usuario registrado
 *    produces:
 *    - application/json
 *    parameters:
 *    - in: body
 *      name: "pedido"
 *      description : PRODUCTO, CANTIDAD, FORMA DE PAGO, DIRECCIÓN, ESTADO
 *      schema:
 *       $ref: "#/definitions/Pedido"
 *    responses:
 *      200:
 *        description: Sucess
 * 
 * definitions:
 *   Pedido:
 *     type: object
 *     required:
 *     - "carrito"
 *     - "formaPago"
 *     - "direccion"
 *     - "estado"
 *     properties:
 *       carrito:
 *         type: array
 *         items:
 *           $ref: "#/definitions/Carrito"
 *       formaPago:
 *         name: formaPago
 *         type: string
 *       direccion:
 *         type: string
 *       estado:
 *         type: string
 * 
 *   Carrito:
 *     type: object
 *     required:
 *     - "detalleCarrito"
 *     properties:
 *       detalleCarrito:
 *         name: detalleCarrito
 *         type: array
 *         items:
 *           $ref: "#/definitions/Detalle_carrito"
 *         description: Array de detalle del carrito
 * 
 *   Detalle_carrito:
 *     type: object
 *     required:
 *     - "producto"
 *     - "cantidad"
 *     properties:
 *       producto:
 *         name: producto
 *         type: array
 *         items:
 *           $ref: "#/definitions/Producto"
 *         description: Nombre del producto
 *       cantidad:
 *         name: cantidad
 *         type: integer
 * 
 *   Producto:
 *     type: object
 *     properties:
 *       id:
 *         type: integer
 *         description: ID del producto
 *       nombreProducto:
 *         type: string
 *         description: Nombre del producto
 *       precio:
 *         type: number
 *         description: Precio
 *       
 */



/** GET **/

/**
 * @swagger
 * /{userId}/pedidos/historial:
 *  get:
 *    tags:
 *    - Pedidos
 *    description: Historial de pedidos por usuario no admin
 *    responses:
 *      200:
 *        Success
 * 
 */


/** PUT **/

/**
 * @swagger
 * /pedidos/{id}:
 *  put:
 *    tags:
 *    - Pedidos
 *    description: Modificar el estado de los pedidos siendo admin
 *    parameters:
 *    - name: id
 *      type: integer
 *      in: formData
 *      required: true
 *    - name: estado
 *      type: string
 *      in: formData
 *      required: true
 *    responses:
 *      200:
 *        Success
 */

/**
 * @swagger
 * /pedidos:
 *  get:
 *    tags:
 *    - Pedidos
 *    description: Listar todos los pedidos desde un usr admin
 *    responses:
 *      200:
 *        Success
 */



/** FORMA DE PAGO **/

/**
 * @swagger
 * /forma_pago/agregar_forma_pago:
 *  post:
 *    tags:
 *    - Forma_pago
 *    description: Registro de nueva forma de pago
 *    parameters:
 *    - name: formaPago
 *      type: string
 *      in: formData
 *      required: false
 *    responses:
 *      200:
 *        Success
 */

/**
 * @swagger
 * /forma_pago:
 *  get:
 *    tags:
 *    - Forma_pago
 *    description: Listado de formas de pago
 *    responses:
 *      200:
 *        Success
 */

/**
 * @swagger
 * /forma_pago/{id}:
 *  put:
 *    tags:
 *    - Forma_pago
 *    description: Editar forma de pago
 *    parameters:
 *    - name: id
 *      type: integer
 *      in: formData
 *      required: true
 *    - name: tipo
 *      type: string
 *      in: formData
 *      required: true
 *    responses:
 *      200:
 *        Success
 */

 /** DELETE **/

 /**
  * @swagger
  * /forma_pago/{id}:
  *  delete:
  *    tags:
  *    - Forma_pago
  *    description: Eliminación de formas de pago
  *    parameters:
  *    - name: id
  *      type: int
  *      in: formData
  *      required: false
  *      description: id de forma de pago
  *    responses:
  *      200:
  *        Sucess
  */